let com = require("serialport");
let child_process = require("child_process");
const Readline = require("@serialport/parser-readline");

let config = {
  intervals: {
    getHeartRateData: 1000,
    attemptReOpen: 1000,
  },
};

module.exports = function (getDataCallback) {
  // open the port
  const port = new com("COM3", {
    baudRate: 9600,
    dataBits: 8,
    parity: "none",
    stopBits: 1,
  });

  // const parser = port.pipe(new Readline({ delimiter: '\r\n' }))

  port.on("open", function () {
    console.log("Port opened.");

    // wait minimum of 250ms for device to power on
    child_process.execSync("ping 127.0.0.1 -n 3 > nul");

    // loop every x ms
    var getHeartDataInterval = setInterval(function () {
      // stop getting heart data if port is not open
      if (!port.isOpen) {
        console.log("Port is not open. Stop getting heart data.");
        clearInterval(getHeartDataInterval);
        return;
      }

      // get heart data
      port.write("G 32\r\n", (err) => {
        if (err) return console.log(`Error sending command to HRMI: ${err.message}`);

        console.log("Sent command 'G' (Get heart rate data).");
      });
    }, config.intervals.getHeartRateData);
  });

  port.on("error", function (err) {
    console.log("Error: ", err.message);
  });

  port.on("disconnect", function () {
    console.log("Disconnected.");

    // try to re open the port every x ms
    var intervalId = setInterval(function () {
      if (port.isOpen()) {
        clearInterval(intervalId);
        return;
      }
      port.open();
    }, config.intervals.attemptReOpen);
  });

  // on data received
  port.on("data", getDataCallback);

  return port;
};
