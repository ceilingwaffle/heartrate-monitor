NOTE: This is barely even an alpha version -- still a huge work in progress!! I'll make this a lot easier to set up in future.


SET UP INSTRUCTIONS:
- Install node.JS for Windows
- unzip the HeartRateMonitor_InterfaceServerCode_v0.1.zip file to the C:\ drive (or somewhere suitable). 
- run cmd.exe (press Win+R, type cmd.exe)
- type    c:
- type    cd c:\HeartRateMonitor
(or whatever you named the folder)
- type    node server.js
This should start the HTTP server on port 1337.
- Create a new browser source in OBS with URL: http://localhost:1337
- Your heart rate will show when 


PROBLEMS?
- If the server isn't able to connect to the HRMI USB card, 
	- Open hrmi.js in notepad
	- Modify "COM3" on line 13: Try "COM1", run "node server.js" again, see if it fixed it. If not, try "COM2", and repeat until you find a working COM port.
