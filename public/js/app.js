var heart = $("#heart");
var animSpeed = 0;

var socket = io.connect("http://127.0.0.1:1337", {
  reconnection: true,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
  reconnectionAttempts: Infinity,
});

const config = {
  maxHeartRateBufferTime: 5000,
};

var lastNonZeroReceivedAtTime = Date.now();

socket.on("heartRate", function (heartRate) {
  console.log("heartRate", heartRate);
});

socket.on("hrmiData", function (data) {
  console.log("data", data);
  var dataArray = data.split(" ");
  var latestBPM = dataArray[2];

  if (!Number.isInteger(parseInt(latestBPM))) latestBPM = 0;
  else latestBPM = parseInt(latestBPM);

  //console.log('hrmiData: ', dataArray);

  // todo: checks for bad status code and handle it
  var statusCode = dataArray[0];
  if (statusCode != "1" || latestBPM >= 180 || latestBPM <= 20) {
    onBadHeartData();
    return;
  }

  lastNonZeroReceivedAtTime = Date.now();
  onGoodHeartData();

  // calculate animation speed value from current heart rate data

  animSpeed = 1 / (latestBPM / 60);
  console.log("latestBPM", latestBPM);
  console.log("animSpeed", animSpeed);

  // update bpm text
  $("#bpm-number").text(Math.round(latestBPM));
});

socket.on("disconnect", function () {
  //$('#bpm-number').text('0');
  $("#bpm-container").css("opacity", 0.4);
});

heart.bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
  // heart.removeClass("heartbeat-animation").addClass("heartbeat-animation");
  var me = this;
  this.style.webkitAnimation = "none";
  setTimeout(function () {
    console.log("animated ended... animSpeed", animSpeed);
    me.style.webkitAnimation = "";
    // animSpeed = Math.random().toFixed(4);
    heart.css("animation", "bigbadaboom " + animSpeed + "s 1");
  }, 10);
});

function onBadHeartData() {
  var now = Date.now();

  if (lastNonZeroReceivedAtTime + config.maxHeartRateBufferTime <= now) {
    $("#bpm-number").text("0");
    $("#bpm-container").css("opacity", 0.5);
    $("#heart").css("opacity", 0.5);
    $("#bpm-number").css("visibility", "hidden");
  }
}

function onGoodHeartData() {
  $("#bpm-number").css("visibility", "visible");
  $("#bpm-container").css("opacity", 1.0);
  $("#heart").css("opacity", 1.0);
}
