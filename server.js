var express = require("express"),
  app = express(),
  http = require("http").Server(app),
  path = require("path"),
  io = require("socket.io")(http),
  hrmi = require("./hrmi.js");

const config = {
  httpPort: 1337,
};

let socket = null;

io.on("connection", function (s) {
  socket = s;
  console.log("a client connected.");

  socket.on("disconnect", function () {
    console.log("a client disconnected.");
  });
});

let dataStrBuffer = "";

let hrmiPort = hrmi(function (hrmiData) {
  // if (socket) {
  if (io) {
    // console.log('socket:',socket);
    var dataStr = ab2str(hrmiData);
    dataStrBuffer += dataStr;

    if (dataStrBuffer.charAt(dataStrBuffer.length - 1) == "\r") {
      console.log("dataStrBuffer", dataStrBuffer);
      var dataArray = dataStrBuffer.split(" ");
      var heartRate = dataArray[2];
      io.emit("heartRate", heartRate);
      io.emit("hrmiData", dataStrBuffer);
      dataStrBuffer = "";
    }
  }
});

// arary buffer to string
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/public/index.html");
});

app.use("/", express.static(path.join(__dirname, "public")));

http.listen(config.httpPort, function () {
  console.log(`Listening on *:${config.httpPort}`);
});
